import { Component, OnInit } from '@angular/core';
import { UsuarioFilter } from 'src/app/core/model/UsuarioFilter';
import { UsuarioService } from '../usuario.service';

@Component({
  selector: 'app-pesquisa-usuario',
  templateUrl: './pesquisa-usuario.component.html',
  styleUrls: ['./pesquisa-usuario.component.css']
})
export class PesquisaUsuarioComponent implements OnInit {

  filtro = new UsuarioFilter();
  usuarios = [];
  totalRegistros = 0;

  constructor(
    private usuarioService: UsuarioService
  ) { }

  situacao = [
    {label: 'Todos', value: ''},
    {label: 'Habilitado', value: 'A'},
    {label: 'Desabilitado', value: 'I'}
  ];

  perfil = [
    {label: 'Todos', value: '-1'},
    {label: 'Aluno', value: '0'},
    {label: 'Gestor Municipal', value: '1'},
    {label: 'Gestor Estadual', value: '2'},
    {label: 'Gestor Nacional', value: '3'}
  ];

  ngOnInit() {
  }

  pesquisar(pagina = 0) {

    this.filtro.pagina = pagina;

    this.usuarioService.listarUsuarios(this.filtro)
      .subscribe(response => {
        this.usuarios = response.content;
        this.totalRegistros = response.totalElements;
      });
  }

}

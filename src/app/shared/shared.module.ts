import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '@angular/cdk/layout';
import { PrimengModule } from './primeng/primeng.module';
import { RouterModule } from '@angular/router';
import { ToastyModule } from 'ng2-toasty';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PrimengModule,
    LayoutModule,
    RouterModule,
    ToastyModule.forRoot()
  ],
  exports: [
    PrimengModule,
    LayoutModule,
    RouterModule,
    ToastyModule
  ]
})
export class SharedModule { }

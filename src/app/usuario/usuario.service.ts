import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { UsuarioFilter } from '../core/model/UsuarioFilter';
import { UsuarioForm } from '../core/model/UsuarioForm';

@Injectable()
export class UsuarioService {

  usuariosUrl: string;

  constructor(private http: HttpClient) {
    this.usuariosUrl = `${environment.apiUrl}/usuarios`;
   }

  listarUsuarios(filtro: UsuarioFilter): Observable<any> {

    const httpOptions = {
      params: new HttpParams()
      .set('nome', filtro.nome === undefined ? '' : filtro.nome)
      .set('situacao', filtro.situacao === undefined ? '' : filtro.situacao)
      .set('perfil', filtro.perfil === undefined ? '-1' : filtro.perfil)
      .set('page', filtro.pagina.toString())
      .set('size', filtro.itensPorPagina.toString())

    };
    return this.http.get(`${this.usuariosUrl}`, httpOptions );
  }

  excluir(codigo: number) {
    return this.http.delete(`${this.usuariosUrl}/${codigo}`);
  }

  mudarSituacao(codigo: number): Observable<any> {
    return this.http.post<any>(`${this.usuariosUrl}/${codigo}`, null);
  }

  criar(usuario: UsuarioForm): Observable<UsuarioForm> {
    usuario.situacao = 'A';
    return this.http.post<UsuarioForm>(`${this.usuariosUrl}`, usuario);
  }

  atualizar(usuario: UsuarioForm, codigo: number, situacao: string): Observable<UsuarioForm> {
    usuario.situacao = situacao;
    return this.http.put<UsuarioForm>(`${this.usuariosUrl}/${codigo}`, usuario);
  }

  buscarPorCodigo(codigo: number): Observable<UsuarioForm> {
    return this.http.get<UsuarioForm>(`${this.usuariosUrl}/${codigo}`);
  }

}

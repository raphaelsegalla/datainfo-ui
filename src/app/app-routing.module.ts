import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PesquisaUsuarioComponent } from './usuario/pesquisa-usuario/pesquisa-usuario.component';
import { CadastraUsuarioComponent } from './usuario/cadastra-usuario/cadastra-usuario.component';

const routes: Routes = [
  {path: '', redirectTo: 'usuarios', pathMatch: 'full'},
  {path: 'usuarios', component: PesquisaUsuarioComponent},
  {path: 'usuarios/novo', component: CadastraUsuarioComponent},
  {path: 'usuarios/:codigo', component: CadastraUsuarioComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { CalendarModule } from 'primeng/calendar';
import { TooltipModule } from 'primeng/tooltip';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputMaskModule } from 'primeng/inputmask';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { PickListModule } from 'primeng/picklist';
import { MenuModule } from 'primeng/menu';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    InputTextModule,
    ButtonModule,
    DropdownModule,
    TableModule,
    TooltipModule,
    MessageModule,
    MessagesModule,
    InputTextareaModule,
    CalendarModule,
    InputMaskModule,
    ConfirmDialogModule,
    PickListModule,
    MenuModule,
  ],
  exports: [
    InputTextModule,
    ButtonModule,
    DropdownModule,
    TableModule,
    TooltipModule,
    MessageModule,
    MessagesModule,
    InputTextareaModule,
    CalendarModule,
    InputMaskModule,
    ConfirmDialogModule,
    PickListModule,
    MenuModule
  ],
  providers: [
    ConfirmationService
  ]
})
export class PrimengModule { }

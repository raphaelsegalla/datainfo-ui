import { Component, OnInit, Input } from '@angular/core';
import { UsuarioFilter } from 'src/app/core/model/UsuarioFilter';
import { PesquisaUsuarioComponent } from '../pesquisa-usuario/pesquisa-usuario.component';
import { UsuarioService } from '../usuario.service';
import { LazyLoadEvent, ConfirmationService } from 'primeng/api';
import { ToastyService } from 'ng2-toasty';

@Component({
  selector: 'app-lista-usuario',
  templateUrl: './lista-usuario.component.html',
  styleUrls: ['./lista-usuario.component.css']
})
export class ListaUsuarioComponent implements OnInit {

  @Input() totalRegistros: number;
  @Input() usuarios = [];
  @Input() filtro = new UsuarioFilter();

  pagina: number;

  constructor(
    private pesquisaUsuario: PesquisaUsuarioComponent,
    private usuarioService: UsuarioService,
    private confirmationService: ConfirmationService,
    private toasty: ToastyService,
  ) { }

  ngOnInit() {
  }

  aoMudarPagina( event: LazyLoadEvent ) {
    const pagina = event.first / event.rows;
    this.pagina = pagina;
    this.pesquisaUsuario.pesquisar(pagina);
  }

  excluir(usuario: any) {
    this.usuarioService.excluir(usuario.codigo)
    .subscribe( () => {
      this.pesquisaUsuario.pesquisar(this.pagina);
      this.toasty.success(`Exclusao efetuda com sucesso`);
    });
  }

  confirmarExclusao(usuario: any) {
    this.confirmationService.confirm({
      message: `Deseja realmente excluir o usuário ${usuario.nome}?`,
      accept: () => {
        this.excluir(usuario);
      }
    });
  }

  ativarOuDesativar(usuario: any) {
    this.usuarioService.mudarSituacao(usuario.codigo)
    .subscribe(() => {
      this.pesquisaUsuario.pesquisar(this.pagina);
      this.toasty.success(`Usuário ${usuario.situacao === 'A' ? 'desabilitado' : 'habilitado'} com sucesso!`);
    });
  }

  confirmarAtivarOuDesativar(usuario: any) {
    this.confirmationService.confirm({
      message: `Tem certeza que deseja "${usuario.situacao === 'A' ? 'Desativar' : 'Ativar'}" o usuario "${usuario.nome}"`,
      accept: () => {
        this.ativarOuDesativar(usuario);
      }
    });
  }

}

export class UsuarioForm {
  email: string;
  nome: string;
  cpf: string;
  telefone: string;
  funcaoUsuarioExterno: string;
  perfil: string;
  situacao: string;
}

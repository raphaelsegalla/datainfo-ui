export class UsuarioFilter {
  nome: string;
  situacao: string;
  perfil: string;
  pagina = 0;
  itensPorPagina = 5;
}

import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsuarioService } from '../usuario.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastyService } from 'ng2-toasty';

@Component({
  selector: 'app-cadastra-usuario',
  templateUrl: './cadastra-usuario.component.html',
  styleUrls: ['./cadastra-usuario.component.css']
})
export class CadastraUsuarioComponent implements OnInit {

  subscription: Subscription;
  usuarioForm: FormGroup;
  valorEditando: boolean;
  codigoUsuario: number;
  situacao: string;

  constructor(
    private usuarioService: UsuarioService,
    private formBuilder: FormBuilder,
    private toasty: ToastyService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  perfis = [
    {label: 'Aluno', value: '0'},
    {label: 'Gestor Municipal', value: '1'},
    {label: 'Gestor Estadual', value: '2'},
    {label: 'Gestor Nacional', value: '3'}
  ];

  funcoes = [
    {label: 'Gestor', value: '1'},
    {label: 'Administrador', value: '2'},
    {label: 'Frente de caixa', value: '3'},
  ];

  ngOnInit() {

    const codigoUsuario = this.route.snapshot.params[`codigo`];

    this.valorEditando = codigoUsuario;
    this.codigoUsuario = codigoUsuario;

    if (codigoUsuario) {
      this.carregarUsuario(codigoUsuario);
    }

    this.usuarioForm = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      nome: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(50)]],
      cpf: [null, [Validators.required]],
      telefone: [null, [Validators.required]],
      funcaoUsuarioExterno: [null, [Validators.required]],
      perfil: [null, [Validators.required]]
    });
  }

  get editando() {
    return Boolean(this.valorEditando);
  }

  carregarUsuario(codigo: number) {
    this.usuarioService.buscarPorCodigo(codigo).subscribe( response => {
      this.situacao = response.situacao;
      this.usuarioForm = this.formBuilder.group({
        email: [response.email, [Validators.required, Validators.email]],
        nome: [response.nome, [Validators.required, Validators.minLength(5), Validators.maxLength(50)]],
        cpf: [response.cpf, [Validators.required]],
        telefone: [response.telefone, [Validators.required]],
        funcaoUsuarioExterno: [null, [Validators.required]],
        perfil: [null, [Validators.required]]
      });
    });
  }

  salvar() {
    if (this.editando) {
      this.atualizarUsuario();
    } else {
      this.adicionarUsuario();
    }
  }

  adicionarUsuario() {

    const valueSubmit = Object.assign({}, this.usuarioForm.value);

    this.subscription = this.usuarioService.criar(valueSubmit)
    .subscribe( response => {
      this.toasty.success(`Cadastro efetuado com sucesso`);
      this.usuarioForm.reset();
    }, err => {
      this.toasty.error(`Erro ao cadastrar usuário`);
    });
  }

  atualizarUsuario() {
    const valueSubmit = Object.assign({}, this.usuarioForm.value);

    this.subscription = this.usuarioService.atualizar(valueSubmit, this.codigoUsuario, this.situacao)
    .subscribe( response => {
      this.router.navigate([`/usuarios`]);
      this.toasty.success(`Alteração efetuada com sucesso`);
    }, err => {
      this.toasty.error(`Erro ao alterar usuário`);
    });
  }

}
